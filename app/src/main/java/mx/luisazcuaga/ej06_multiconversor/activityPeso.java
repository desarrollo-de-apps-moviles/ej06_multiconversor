package mx.luisazcuaga.ej06_multiconversor;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class activityPeso extends AppCompatActivity {

    Button idConvertirKilos, idConvertirLibras;
    TextView idKilos, idLibras;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_peso);

        idKilos = findViewById(R.id.editTextKilos);
        idLibras = findViewById(R.id.editTextLibras);

        idConvertirKilos = findViewById(R.id.buttonConvertirKilos);
        idConvertirLibras = findViewById(R.id.buttonConvertirLibras);

        idConvertirKilos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String kilos = idKilos.getText().toString();
                double cantidadkilos = Double.parseDouble(kilos);

                Double libras = cantidadkilos*2.205;
                String cantidadlibras = String.valueOf(libras);

                idLibras.setText(cantidadlibras);
            }
        });

        idConvertirLibras.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String libras = idLibras.getText().toString();
                double cantidadlibras = Double.parseDouble(libras);

                Double kilos = cantidadlibras/2.205;
                String cantidadkilos = String.valueOf(kilos);

                idKilos.setText(cantidadkilos);
            }
        });




    }
}
