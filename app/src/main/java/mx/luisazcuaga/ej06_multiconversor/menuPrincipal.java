package mx.luisazcuaga.ej06_multiconversor;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class menuPrincipal extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu_principal);
    }

    public void goLongitud(View view) {
        Intent intent = new Intent(this, activityLongitud.class);
        startActivity(intent);
    }

    public void goPeso(View view) {
        Intent intent = new Intent(this, activityPeso.class);
        startActivity(intent);
    }

    public void goTemperatura(View view) {
        Intent intent = new Intent(this, activityTemp.class);
        startActivity(intent);
    }

    public void exit(View view) {
        //finish();
        //System.exit(0);
        this.finish();
    }

}
