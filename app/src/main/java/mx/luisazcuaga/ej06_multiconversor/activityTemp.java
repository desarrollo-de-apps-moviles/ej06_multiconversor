package mx.luisazcuaga.ej06_multiconversor;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class activityTemp extends AppCompatActivity {

    Button idConvertirCelsius, idConvertirFah;
    TextView idCelsius, idFahrenheit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_temp);

        idCelsius = findViewById(R.id.editTextCelius);
        idFahrenheit = findViewById(R.id.editTextFah);

        idConvertirCelsius = findViewById(R.id.buttonConvertirCelsius);
        idConvertirFah = findViewById(R.id.buttonConvertirFah);

        idConvertirCelsius.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String celsius = idCelsius.getText().toString();
                double cantidadcelsius = Double.parseDouble(celsius);

                Double fahrenheit = (cantidadcelsius * 9 / 5) + 32;
                String cantidadfahrenheit = String.valueOf(fahrenheit);

                idFahrenheit.setText(cantidadfahrenheit);
            }
        });

        idConvertirFah.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String fahrenheit = idFahrenheit.getText().toString();
                double cantidadfahrenheit = Double.parseDouble(fahrenheit);

                Double celsius = (cantidadfahrenheit - 32) * 5 / 9;
                String cantidadcelsius = String.valueOf(celsius);

                idCelsius.setText(cantidadcelsius);
            }
        });

    }
}
