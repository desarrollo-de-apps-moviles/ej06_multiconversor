package mx.luisazcuaga.ej06_multiconversor;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class activityLongitud extends AppCompatActivity {

    Button idConvertirMillas, idConvertirKM;
    TextView idKm, idMillas;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_longitud);

        idConvertirMillas = findViewById(R.id.buttonConvertirFah);
        idConvertirKM = findViewById(R.id.buttonConvertirKM);

        idKm = findViewById(R.id.editTextKM);
        idMillas = findViewById(R.id.editTextMillas);

        idConvertirMillas.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String millas = idMillas.getText().toString();
                double cantidadmillas = Double.parseDouble(millas);

                Double kilometros = cantidadmillas*1.604;
                String cantidadkilometros = String.valueOf(kilometros);

                idKm.setText(cantidadkilometros);
            }
        });

        idConvertirKM.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String kilometros = idMillas.getText().toString();
                double cantidadkilometros = Double.parseDouble(kilometros);

                Double millas = cantidadkilometros/1.604;
                String cantidadmillas = String.valueOf(millas);

                idMillas.setText(cantidadmillas);
            }
        });
    }


}
